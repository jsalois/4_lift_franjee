package ca.polymtl.log4420
package test
package unitaire

import org.scalatest._
import matchers.ShouldMatchers

@EquipeDeDeux( pointsDeux = 30, pointsQuatre = 15)
class ProgrammeGenieTestLift
  extends FeatureSpec
  with ShouldMatchers
  with TestLiftSession
  with TestCheminementMock
{
  feature( "[A] Afficher la liste des cheminements officiels offerts par Polytecnique" )
  {
    info("En tant qu'étudiant à Polytechnique")
    info("Je veux voir les cheminements des programmes de génie")
    info("Pour choisir un cheminement de référence")

    scenario("")
    {

    }
  }
}