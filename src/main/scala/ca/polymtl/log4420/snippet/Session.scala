package ca.polymtl.log4420
package snippet

import net.liftweb._
import util.CssSel
import util.Helpers._

import xml.NodeSeq

object Session
{
  def apply( session: model.Session ): CssSel =
  {
    ".cours" #> session.cours.map( cours => Cours( cours ) ) &
    ".periode *" #> session.periode.toString &
    ".annee *" #> session.annee.toString &
    ".total-credits *" #> session.totalCredits.toString
  }
}