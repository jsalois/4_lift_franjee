package ca.polymtl.log4420
package snippet

// Lift
import net.liftweb._
import common.Full
import sitemap.Loc.Template
import sitemap.{*, Menu}
import http._
import util.Helpers._

import xml.NodeSeq

object Cheminement
{
  // permet de convertir un url dans un cheminement
  // url: cheminement / comite / titre cheminement / annee debut
  // https://github.com/MasseGuillaume/SEO-Friendly-Menu-Params
  val menuComite = Menu.params[ (model.Cheminement) ](
    "CheminementComite",                                                                   // name
    "Cheminement Comité",                                                                  // link text
    { case model.Cheminements( parsedCheminement ) => Full( parsedCheminement ) },         // List[String] => model
    cheminement => model.Cheminements.path( cheminement )                                  // model => List[String]
  ) / "Cheminement" / * / * / * >>                                                         // url: Cheminement / Logiciel / Multimedia / 2009
  Template( () => Templates( "Cheminement" :: Nil ) openOr <b>template not found</b> )     // load template: webapp / Cheminement.html

  lazy val comite = menuComite.toLoc                                                       // loc to add in sitemap

  val menuCheminement = Menu.param[ (model.Cheminement) ](                                 // ibid
    "Cheminement",
    "Cheminement",
    { case model.Cheminements( parsedCheminement ) => Full( parsedCheminement ) },
    cheminement => cheminement.hashCode.toString
  ) / "Cheminement" / * >>                                                                 // url: Cheminement / cheminement.hashcode
  Template( () => Templates( "Cheminement" :: Nil ) openOr <b>template not found</b> )

  lazy val cheminement = menuCheminement.toLoc
}

class Cheminement( cheminement: model.Cheminement ) extends DispatchSnippet
{
  // pour eviter de faire de la reflexion dans le html
  // ( https://www.assembla.com/spaces/liftweb/wiki/More_on_Snippets )
  def dispatch = { case "render" => render }

  def render: NodeSeq => NodeSeq =
  {                                                                                      // when url is Cheminement / Logiciel / Multimedia / 2009
    ".addresse [href]" #> S.request.map( _.uri ) &                                       // showes Cheminement / Logiciel / Multimedia / 2009
    ".addresse2 [href]" #> ( "/Cheminement/" + cheminement.hashCode.toString ) &         // showes Cheminement / cheminement.hashcode
    ".session *" #> cheminement.sessions.map( session => Session( session ) ) // &
    // ".ajouter-session" #> ...
    // ".ajouter-cours" #> ...
    // etc
  }
}